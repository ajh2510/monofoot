﻿#region Using Statements
using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Media;
using Microsoft.Xna.Framework.Storage;
using Microsoft.Xna.Framework.GamerServices;
using System.Collections;
#endregion


namespace TeacherBase
{
    class Ant : Creature
    {
        private bool carryingFood = false;
        public Ant(string newTexture, AntHill home): base(newTexture)
        {
            SetHomeHill(home);
        }

        public Ant(AntHill home)
            : base()
        {
            SetHomeHill(home);
        }

        public override void Update()
        {
            if (carryingFood)
            {
                WalkTowardsHome();
                CheckHome();
            }
            else
            {
                SearchForFood();
            }
        }

        private void CheckHome()
        {
            if (AtHome())
            {
                DropFood();
            }
        }

        public void CheckFood()
        {
            ArrayList closeObjects = myWorld.GetObjectsWithinDistance(12, true, this);
            foreach (Object o in closeObjects)
            {
                String objectType = o.GetType().Name;
                if (objectType.Equals("Food"))
                {
                    Food food = (Food)o;
                    TakeFood(food);
                }
            }
            
        }

        private void TakeFood(Food food)
        {
            carryingFood = true;
            food.TakeSome();
            SetTexture(myWorld.GetTexture("ant-with-food"));
        }

        private void SearchForFood()
        {
            RandomWalk();
            CheckFood();
        }

        private bool AtHome()
        {
            if (GetHomeHill() != null)
            {
                return (Math.Abs(GetX() - GetHomeHill().GetX()) < 4) && (Math.Abs(GetY() - GetHomeHill().GetY()) < 4);
            }
            else
            {
                return false;
            }

        }

        private void DropFood()
        {
            carryingFood = false;
            GetHomeHill().CountFood();
            SetTexture(myWorld.GetTexture("ant"));
        }

    }

    
}
