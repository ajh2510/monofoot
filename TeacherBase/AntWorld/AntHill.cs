﻿#region Using Statements
using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Media;
using Microsoft.Xna.Framework.Storage;
using Microsoft.Xna.Framework.GamerServices;
using System.Reflection;
using System.Security.Permissions;
using System.Collections;
#endregion

namespace TeacherBase
{
    class AntHill : Character
    {

        private int ants = 0;

        /** Total number of ants in this hill. */
        private int maxAnts = 40;
        Random rnd1 = new Random();
        /** Counter to show how much food have been collected so far. */
        private Counter foodCounter;

        public AntHill(string newTexture): base(newTexture)
        {

        }

        public AntHill(string newTexture, int newX, int newY)
            : base(newTexture, newX, newY)
        {

        }

        public override void Update(){
            if (ants < maxAnts)
            {
                if (rnd1.Next(0,100) < 10)
                {
                    Ant newAnt = new Ant("Ant",this);
                    myWorld.AddCharacter(newAnt);
                    newAnt.SetLocation(this.GetX(), this.GetY());

                    ants++;
                }
            }

        }

        public void CountFood()
        {
            // if we have no food counter yet (first time) -- create it
            if (foodCounter == null)
            {
                foodCounter = new Counter("Food: ", (int)GetX(), ((int)GetY()));
                GetWorld().AddCharacter(foodCounter);
            }
            foodCounter.Increment();
        }


    }
}
