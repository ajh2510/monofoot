﻿#region Using Statements
using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Media;
using Microsoft.Xna.Framework.Storage;
using Microsoft.Xna.Framework.GamerServices;
using System.Reflection;
using System.Security.Permissions;
using System.Collections;
#endregion

namespace TeacherBase
{
    class AntWorld : World
    {
        public AntWorld(int horizGridNum, int vertGridNum, int gridSize)
            : base(horizGridNum, vertGridNum, gridSize)
        {
            setBackground("sand");
        }

        public override void Populate()
        {

            AddCharacter(new AntHill("AntHill",200,200));
            AddCharacter(new Food(300, 300));
            //guiManager.AddCharacterData(new CharacterDragData(new AntHill("AntHill"), GetTexture("AntHill"), "AntHill"));
            //guiManager.AddCharacterData(new CharacterDragData(new Lobster("Lobster"), GetTexture("Lobster"), "Lobster"));

            //Type[] theList = Assembly.GetExecutingAssembly().GetTypes();
            //for (int i = 0; i < theList.Length; i++)
            //{
            //    Console.WriteLine(theList[i].ToString());
            //}
        }
    }
}
