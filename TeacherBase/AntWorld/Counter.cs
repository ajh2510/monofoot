﻿#region Using Statements
using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Media;
using Microsoft.Xna.Framework.Storage;
using Microsoft.Xna.Framework.GamerServices;
using System.Reflection;
using System.Security.Permissions;
using System.Collections;
#endregion
namespace TeacherBase
{
    class Counter : Character
    {
        public int value = 0;
        public String text = "";
        public Counter():this("")
        {
        }

        public Counter(string prefix)
        {
            text = prefix;
        }

        public Counter(int newX, int newY){
            coordinates = new Vector2(newX, newY);
            text = "";
        }

        public Counter(string prefix, int newX, int newY):this(newX,newY){
            text = prefix;
        }

        public void Increment()
        {
            value++;
        }

        public override void AddedToWorld()
        {
            int imageWidth = (text.Length + 2) * 10;
            base.AddedToWorld();
        }

    }
}
