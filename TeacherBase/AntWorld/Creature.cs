﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TeacherBase
{
    class Creature : Character
    {
        private static int SPEED = 3;

        /** Current movement. Defined as the offset in x and y direction moved in each step. */
        private int deltaX;
        private int deltaY;
        Random rnd1 = new Random();
        private AntHill home;

        public Creature()
        {
            deltaX = 0;
            deltaY = 0;
        }

        public Creature(string newTexture): base(newTexture)
        {
            deltaX = 0;
            deltaY = 0;
        }

        public void SetHomeHill(AntHill homeHill)
        {
            home = homeHill;
        }

        public AntHill GetHomeHill()
        {
            return home;
        }
        public void RandomWalk()
        {
            if (RandomChance(50)) {
                deltaX = AdjustSpeed(deltaX);
                deltaY = AdjustSpeed(deltaY);
            }
            Walk();
        }

        public void Walk()
        {
            SetLocation(GetX() + deltaX, GetY() + deltaY);
            SetRotation((int)(180 * Math.Atan2(deltaY, deltaX) / Math.PI));
            int boundaryWidth = 5;
            if (GetX() < boundaryWidth)
            {
                SetLocation(boundaryWidth, GetY());
            }

            if (GetX() > myWorld.GetWidth() - boundaryWidth)
            {
                SetLocation(myWorld.GetWidth() - boundaryWidth, GetY());
            }
            if (GetY() < boundaryWidth)
            {
                SetLocation(GetX(), boundaryWidth);
            }

            if (GetY() > myWorld.GetHeight() - boundaryWidth)
            {
                SetLocation(GetX(), myWorld.GetHeight() - boundaryWidth);
            }
        }

        public void WalkTowardsHome()
        {
            if (home == null)
            {
                //if we do not have a home, we can not go there.
                return;
            }
            if (RandomChance(2))
            {
                RandomWalk();  // cannot always walk straight. 2% chance to turn off course.
            }
            else
            {
                HeadRoughlyTowards(home);
                Walk();
            }
        }
    

        public void WalkAwayFromHome()
        {
            if (home == null)
            {
                //if we do not have a home, we can not head away from it.
                return;
            }
            if (RandomChance(2))
            {
                RandomWalk();  // cannot always walk straight. 2% chance to turn off course.
            }
            else
            {
                HeadRoughlyTowards(home);   // first head towards home...
                deltaX = -deltaX;           // ...then turn 180 degrees
                deltaY = -deltaY;
                Walk();
            }
        }

        public void HeadTowards(Character target)
        {
            deltaX = CapSpeed((int)(target.GetX() - GetX()));
            deltaY = CapSpeed((int)(target.GetY() - GetY()));
        }

        private void HeadRoughlyTowards(Character target)
        {


            int distanceX = Math.Abs((int)GetX() - (int)target.GetX());
            int distanceY = Math.Abs((int)GetY() - (int)target.GetY());
            bool moveX = (distanceX > 0) && (rnd1.Next(distanceX + distanceY) < distanceX);
            bool moveY = (distanceY > 0) && (rnd1.Next(distanceX + distanceY) < distanceY);

            deltaX = ComputeHomeDelta(moveX, (int)GetX(), (int)target.GetX());
            deltaY = ComputeHomeDelta(moveY, (int)GetY(), (int)target.GetY());
        }



        private int ComputeHomeDelta(bool move, int current, int home)
        {
            if (move)
            {
                if (current > home)
                    return -SPEED;
                else
                    return SPEED;
            }
            else
                return 0;
        }

        private int AdjustSpeed(int speed)
        {
            speed = speed + rnd1.Next(2 * SPEED -1) - SPEED + 1;
            return CapSpeed(speed);
        }

        private int CapSpeed(int speed)
        {
            if (speed < -SPEED)
                return -SPEED;
            else if (speed > SPEED)
                return SPEED;
            else
                return speed;
        }

        private bool RandomChance(int percent)
        {
            int newRand = rnd1.Next(0, 100);
            return newRand < percent;
        }
    }
}
