﻿#region Using Statements
using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Media;
using Microsoft.Xna.Framework.Storage;
using Microsoft.Xna.Framework.GamerServices;
using System.Collections;
#endregion



namespace TeacherBase
{
    class Food : Character
    {
        private static int SIZE = 60;
        private static int HALFSIZE = SIZE / 2;
        private static Color color1 = new Color(160, 200, 60);
        private static Color color2 = new Color(80, 100, 30);
        private static Color color3 = new Color(10, 50, 0);

        private static Random randomizer = new Random();
    
        private int crumbs = 100;  // number of bits of food in this pile

        public Food()
        {

        }
        public Food(int newX, int newY): base(newX, newY)
        {

        }

        public override void AddedToWorld()
        {
            UpdateImage();
            base.AddedToWorld();
        }

        public void TakeSome()
        {
            crumbs = crumbs - 3;
            if (crumbs <= 0)
            {
                myWorld.RemoveCharacter(this);
            }
            else
            {
                UpdateImage();
            }
        }

        public void UpdateImage()
        {
            texture = new Texture2D(myWorld.graphics.GraphicsDevice, SIZE, SIZE);
            Color[] imageData = new Color[texture.Width * texture.Height];
            texture.GetData<Color>(imageData);
            for (int i = 0; i < crumbs; i++)
            {
                int x = RandomCoord();
                int y = RandomCoord();
                imageData[y * texture.Height + x] = color1;
                imageData[(y * texture.Height + x) + texture.Width] = color2;
                imageData[(y * texture.Height) + x  +1] = color2;
                imageData[((y * texture.Height) + x + 1) + texture.Width] = color3;
            }
            texture.SetData<Color>(imageData);
            SetTexture(texture);
        }

        public int RandomCoord()
        {
            int val = HALFSIZE + (int)(randomizer.Next(0, HALFSIZE/2) );

            if (val < 0)
                return 0;

            if (val > SIZE - 2)
                return SIZE - 2;
            else
                return val;
        }


    }
}
