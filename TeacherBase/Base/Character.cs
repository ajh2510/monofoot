﻿#region Using Statements
using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Storage;
using Microsoft.Xna.Framework.GamerServices;
using System.Collections;
using System.Reflection;
#endregion

namespace TeacherBase
{
    public class Character :ICloneable
    {

        protected Texture2D texture= null;
        protected string textureName;
        public World myWorld;
        public float rotation;
        public string[] worlds = { };

        protected Vector2 coordinates;
        public Character(string newTexture)
        {
            coordinates = new Vector2(0, 0);
            textureName = newTexture;
            rotation = 0;
        }

        public Character()
        {
            coordinates = new Vector2(0, 0);
            textureName = "";
            
            rotation = 0;
        }

        public Character(string newTexture, int newX, int newY)
        {
            coordinates = new Vector2(newX, newY);
            textureName = newTexture;
        }

        public Character(int newX, int newY)
        {
            coordinates = new Vector2(newX, newY);
            textureName = "";
        }

        public virtual void Update()
        {
        }

        public Vector2 GetCoordinates()
        {
            return coordinates;
        }


        public Texture2D GetTexture()
        {
            return texture;
        }

        protected World GetWorld() { return myWorld; }

        public void SetLocation(float x, float y)
        {
            coordinates = new Vector2(x, y);
        }

        public float GetX()
        {
            return coordinates.X;
        }

        public float GetY()
        {
            return coordinates.Y;
        }

        public float GetRotation()
        {
            return rotation;
        }


        public String GetTextureString()
        {
            return textureName;
        }

        public void SetTexture(Texture2D newTexture)
        {
            texture = newTexture;
        }

        public void SetRotation(float newRotation)
        {
            newRotation /= (180.0f / 3.140f);
            rotation = newRotation;
        }


        public override bool Equals(System.Object obj)
        {
            // If parameter is null return false.
            if (obj == null)
            {
                return false;
            }

            // If parameter cannot be cast to Point return false.
            Character p = obj as Character;
            if ((System.Object)p == null)
            {
                return false;
            }

            // Return true if the fields match:
            return coordinates.Equals(p.GetCoordinates());
        }

        public virtual void AddedToWorld()
        {
            if (textureName.Equals(""))
            {
                string className = this.GetType().Name;
                Texture2D curTexture = myWorld.GetTexture(className);
                if (curTexture != null)
                {
                    SetTexture(curTexture);
                    textureName = className;
                }
            }
        }

        public void RemovedFromWorld()
        {

        }

        public MethodInfo[] GetMethodData()
        {
            return this.GetType().GetMethods();
        }



        public ArrayList GetIntersectingObjects()
        {
            return myWorld.getObjectsAt((int)GetX(), (int)GetY());
        }

        public ArrayList GetNeighbours(int distance, bool diagonal)
        {
            return myWorld.GetObjectsWithinDistance(distance, diagonal, this);
        }

        public ArrayList GetObjectsAtOffset(int dx, int dy)
        {
            return myWorld.getObjectsAt((int)GetX() + dx, (int)GetY() + dy);
        }

        public ArrayList GetObjectsInRange(int r)
        {
            return myWorld.GetObjectsWithinDistance(r, false, this);
        }

        public Character GetOneIntersectingObject()
        {
            return myWorld.GetOneIntersectingObject(this);
        }

        protected bool Intersects(Character c)
        {
            return c.GetX() == GetX() && c.GetY() == GetY();
        }

        public object Clone()
        {
            return this.MemberwiseClone();
        }

        public bool clickIntersects(MouseState m)
        {
            if (GetTexture() == null)
                return false;

            Point adjustedMouse = new Point(m.X - (int)GetX() - (GetTexture().Width / 2), m.Y - (int)GetY() - (GetTexture().Height/ 2));
            double dist = Math.Sqrt((adjustedMouse.X * adjustedMouse.X) + (adjustedMouse.Y* adjustedMouse.Y));
            double bounds = (GetTexture().Width + GetTexture().Height) / 3;
            return dist < bounds;
        }

    }
}
