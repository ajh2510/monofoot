﻿#region Using Statements
using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Media;
using Microsoft.Xna.Framework.Storage;
using Microsoft.Xna.Framework.GamerServices;
using System.Reflection;
using System.Security.Permissions;
using System.Collections;
#endregion

namespace TeacherBase
{
    public class CharacterDragData
    {
        public Character characterData;
        public Texture2D dragTexture;
        public String characterName;
        public Rectangle boundingBox = new Rectangle();
        public CharacterDragData(Character c, Texture2D dragPicture, String nameOfCharacterType)
        {
            characterData = c;
            dragTexture = dragPicture;
            characterName = nameOfCharacterType;
        }

    }
}
