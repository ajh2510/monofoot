﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading;

namespace TeacherBase
{
    class CharacterFunction
    {
        private MethodInfo methodInfo;
        private Rectangle location;
        private SpriteBatch sb;
        private GUIManager guiManager;
        private string fullName;
        private Character attachedCharacter;

        private String[] ValidTypeStrings = { "Single", "Int32", "String" };
        public CharacterFunction(MethodInfo m, int x, int y, int width, int height,GUIManager gm, Character c)
        {
            attachedCharacter = c;
            methodInfo = m;
            location = new Rectangle(x, y, width, height);
            guiManager = gm;
            createFullName();
        }

        public void createFullName()
        {
            fullName = methodInfo.ReturnType.Name + " " + methodInfo.Name + "(";
            ParameterInfo[] arguments = methodInfo.GetParameters();
            for (int i = 0; i < arguments.Length; i++)
            {
                fullName += arguments[i].Name;
                if (i != arguments.Length - 1)
                {
                    fullName += ",";
                }
            }
            fullName += ")";
        }



        public void Draw(SpriteBatch newSpriteBatch)
        {
            sb = newSpriteBatch;
            DrawMenuString(location.X, location.Y, methodInfo);
        }

        private void DrawMenuString(int textX, int textY, MethodInfo toWrite)
        {
            Texture2D backgroundTexture = guiManager.GetTextureOfColorAndSize(location.Width, location.Height, Color.Gray);
            sb.Draw(backgroundTexture, new Rectangle(textX, textY, backgroundTexture.Width, backgroundTexture.Height), null, Color.White,
                            0.0f,     //angle of line (calulated above)
                            new Vector2(0, 0), // point in line about which to rotate
                            SpriteEffects.None,
                            0);
            //sb.DrawString(world.font, toWrite, new Vector2(textX, textY), Color.Black);
            sb.DrawString(guiManager.GetFont(), fullName, new Vector2(textX, textY), Color.Black, 0.0f, new Vector2(0, 0), 0.5f, SpriteEffects.None, 0.0f);
        }


        public bool clickIntersects(int x, int y)
        {
            return location.Contains(new Point(x, y));
        }

        public void CallFunction()
        {
            ParameterInfo[] parameters = methodInfo.GetParameters();
            object result = null;
            if (parameters.Length == 0)
            {
                result = methodInfo.Invoke(attachedCharacter, null);
            }
            else
            {

                List<String> s = new List<String>();
                bool works = true;
                for (int i = 0; i < parameters.Length; i++)
                {
                    String[] splitString = (parameters[i].ParameterType.ToString()).Split('.');
                    String finalType= splitString[splitString.Length-1];
                    if(!ValidTypeStrings.Contains<String>(finalType)){
                        Console.WriteLine("method contains untypeable type:" + finalType);
                        works = false;
                        break;
                    }
                    else
                    {
                        s.Add(finalType + " " + parameters[i].Name);
                    }
                }
                if (works)
                {
                    new Thread(delegate()
                    {
                        CallParameteredFunction(s);
                    }).Start();
                }

            }

            if (methodInfo.ReturnType != null)
            {
                PrintFunctionResult(result, methodInfo.ReturnType);
            }
            guiManager.displayingCharacterMenu = false;
        }

        public void CallParameteredFunction(List<String> s)
        {
            List<Object> results = new List<Object>();
            for (int i = 0; i < s.Count; i++)
            {
                String[] splitString = s[i].Split(' ');
                Console.WriteLine("Please Type in data for " + s[i]);
                String input = Console.ReadLine();
                Object o = new Object();
                if (splitString[0].Equals("Single"))
                {
                    o = Single.Parse(input);
                }
                else if (splitString[0].Equals("Int32"))
                {
                    o = Int32.Parse(input);
                }
                else if (splitString[0].Equals("String"))
                {
                    o = input;
                }
                else
                {
                    Console.WriteLine("invalid input");
                }

                results.Add(o);
            }

            object output = null;
            output = methodInfo.Invoke(attachedCharacter, results.ToArray());
            if (methodInfo.ReturnType != null)
            {
                PrintFunctionResult(output, methodInfo.ReturnType);
            }
        }

        public void PrintFunctionResult(object result, Type type)
        {
            Console.WriteLine(result);
        }
    }
}
