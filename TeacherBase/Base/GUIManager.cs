﻿#region Using Statements
using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Media;
using Microsoft.Xna.Framework.Storage;
using Microsoft.Xna.Framework.GamerServices;
using System.Reflection;
using System.Security.Permissions;
using System.Collections;
#endregion

namespace TeacherBase
{
    public class GUIManager
    {
        ArrayList classList;//made of characterdragdata objects
        World world;
        SpriteBatch sb;
        SpriteFont font;
        MouseState oldState = new MouseState();
        CharacterDragData heldCharacter = null;
        Rectangle screenBounds = new Rectangle(50, 50, 400, 400);
        ArrayList nonCharacterClasses;
        ArrayList characterFunctions = new ArrayList();
        int xCoordObject = 600;
        int characterFunctionWidth=200,characterFunctionHeight=12;
        Character currentMenuCharacter;

        Point displayingMenuCoordinates;
        public bool displayingCharacterMenu = false;
        string displayedCharacterName = "";

        public GUIManager(World w)
        {
            classList = new ArrayList();
            nonCharacterClasses = new ArrayList();
            world = w;
        }

        public void Update()
        {
            MouseState newState = world.newState;
            int mouseX = newState.X;
            int mouseY = newState.Y;
            if (newState.RightButton == ButtonState.Pressed && oldState.RightButton == ButtonState.Released)
            {
                
                foreach (var o in classList)
                {
                    CharacterDragData cdd = (CharacterDragData)o;
                    if (cdd.boundingBox.Contains((float)mouseX, (float)mouseY))
                    {
                        heldCharacter=cdd;
                    }
                    
                }
            }else if (newState.RightButton == ButtonState.Released && oldState.RightButton == ButtonState.Released){
                if(screenBounds.Contains((float)mouseX, (float)mouseY) && heldCharacter !=null){
                    PlaceCharacter(heldCharacter.characterData, mouseX, mouseY);
                }
                heldCharacter = null;
            }

            if (displayingCharacterMenu && oldState.LeftButton == ButtonState.Pressed && newState.LeftButton == ButtonState.Released)
            {
                foreach (var cf in characterFunctions)
                {
                    CharacterFunction currentFunction = (CharacterFunction)cf;
                    if (currentFunction.clickIntersects(newState.X, newState.Y))
                    {
                        currentFunction.CallFunction();
                    }
                }
            }

            oldState = newState;
        }

        public void AddCharacterData(CharacterDragData cd)
        {
            classList.Add(cd);
        }

        public void AddNonCharacterClass(String className)
        {
            nonCharacterClasses.Add(className);
        }

        private void PlaceCharacter(Character character, int xCoord, int yCoord)
        {
            world.playFanfare();
            character.SetLocation((float)xCoord, (float)yCoord);
            world.AddCharacter((Character)character.Clone());
            
        }

        public void Draw(SpriteBatch newSpriteBatch)
        {
            int yMoveIncrement = 40;
            sb = newSpriteBatch;
            int startingY = 60;
            CharacterDragData cdd= new CharacterDragData(new Character(),null,"");
            world.DrawRectangle(new Vector2(xCoordObject - 10, 50), new Vector2(200, 10 + (yMoveIncrement * classList.Count)), world.rectColor, true);
            foreach (var o in classList){
                cdd = (CharacterDragData)o;
                DrawString(xCoordObject, startingY, cdd.characterName, ref cdd, Color.LightGreen);
                startingY += yMoveIncrement;
            }

            

            startingY = 510;
            world.DrawRectangle(new Vector2(xCoordObject - 10, 500), new Vector2(200, (10 + ((nonCharacterClasses.Count) * yMoveIncrement))), world.rectColor, true);
            foreach (var o in nonCharacterClasses)
            {
                String s = (String)o;
                cdd = new CharacterDragData(new Character(), null, "");
                DrawString(xCoordObject, startingY, s, ref cdd, Color.LightSkyBlue);
                startingY += yMoveIncrement;
            }
            

            if (heldCharacter != null)
            {
                sb.Draw(heldCharacter.dragTexture, new Rectangle(oldState.X, oldState.Y, heldCharacter.dragTexture.Width, heldCharacter.dragTexture.Height), null, Color.White,
                            0.0f,     //angle of line (calulated above)
                            new Vector2(0, 0), // point in line about which to rotate
                            SpriteEffects.None,
                            0);
            }

            if (displayingCharacterMenu)
            {
                foreach (var cf in characterFunctions)
                {
                    ((CharacterFunction)cf).Draw(sb);
                }
            }


           
        }

        private void DrawString(int textX, int textY, string toWrite, ref CharacterDragData cdd, Color backgroundFill)
        {
            Vector2 textSize= world.font.MeasureString(toWrite);
            if(cdd!=null)
            cdd.boundingBox = new Rectangle(textX, textY, (int)textSize.X, (int)textSize.Y);
            Texture2D backgroundTexture = GetTextureOfColorAndSize((int)textSize.X, (int)textSize.Y, backgroundFill);
            sb.Draw(backgroundTexture, new Rectangle(textX-3,textY-3, backgroundTexture.Width+6, backgroundTexture.Height+6), null, Color.White,
                            0.0f,     //angle of line (calulated above)
                            new Vector2(0, 0), // point in line about which to rotate
                            SpriteEffects.None,
                            0);
            sb.DrawString(world.font, toWrite, new Vector2(textX, textY), Color.Black);
        }


        private void DrawMenuString(int textX, int textY, MethodInfo toWrite)
        {
            Texture2D backgroundTexture = GetTextureOfColorAndSize(200, 12, Color.Gray);
            sb.Draw(backgroundTexture, new Rectangle(textX, textY, backgroundTexture.Width, backgroundTexture.Height), null, Color.White,
                            0.0f,     //angle of line (calulated above)
                            new Vector2(0, 0), // point in line about which to rotate
                            SpriteEffects.None,
                            0);
            //sb.DrawString(world.font, toWrite, new Vector2(textX, textY), Color.Black);
            sb.DrawString(world.font, toWrite.Name, new Vector2(textX, textY), Color.Black, 0.0f, new Vector2(0, 0), 0.5f, SpriteEffects.None, 0.0f);
        }

        public void DisplayCharacterMenu(MethodInfo[] methods, string className, Character menuCharacter)
        {
            currentMenuCharacter = menuCharacter;
            characterFunctions = new ArrayList();
            displayingMenuCoordinates = new Point(world.newState.X, world.newState.Y);
            displayingCharacterMenu = true;
            displayedCharacterName = className;
            int currentY = displayingMenuCoordinates.Y;
            int xIncrement = 210;
            int columnSize = 20;
            for (int i = 0; i < methods.Length; i++)
            {
                if (i % columnSize == 0)
                {
                    currentY = displayingMenuCoordinates.Y;
                }
                characterFunctions.Add(new CharacterFunction(methods[i], displayingMenuCoordinates.X + (xIncrement * (i / columnSize)), currentY, characterFunctionWidth, characterFunctionHeight, this,currentMenuCharacter));
                currentY += 13;

            }
            

        }

        public Texture2D GetTextureOfColorAndSize(int length, int width, Color c)
        {
            Texture2D backgroundTexture = new Texture2D(world.graphics.GraphicsDevice, length, width);
            Color[] imageData = new Color[backgroundTexture.Width * backgroundTexture.Height];
            backgroundTexture.GetData<Color>(imageData);
            for (int i = 0; i < backgroundTexture.Width; i++)
            {
                for (int j = 0; j < backgroundTexture.Height; j++)
                {
                    imageData[(j * backgroundTexture.Width) + i] = c;
                }
            }
            backgroundTexture.SetData<Color>(imageData);
            return backgroundTexture;
        }

        public SpriteFont GetFont()
        {
            return world.font;
        }


    }
}
