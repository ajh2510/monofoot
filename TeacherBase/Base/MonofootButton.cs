﻿#region Using Statements
using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Storage;
using Microsoft.Xna.Framework.GamerServices;
#endregion


namespace TeacherBase
{
    public class MonofootButton
    {
        public bool isStopped;

        public int X, Y, Width, Height;
        public MonofootButton()
        {
            isStopped = false;
            X = 50;
            Y = 500;
            Width = 80;
            Height = 80;
        }


        public void onStop()
        {
            isStopped = true;
        }

        public void onPlay()
        {
            isStopped = false;
        }

        public bool isInBounds(MouseState oldState, MouseState newState)
        {
            return (this.X < oldState.X && oldState.X < this.X + this.Width && this.Y < oldState.Y && oldState.Y < this.Y + this.Height &&
                   this.X < newState.X && newState.X < this.X + this.Width && this.Y < newState.Y && newState.Y < this.Y + this.Height);
        }


    }
}
