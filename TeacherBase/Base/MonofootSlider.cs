﻿#region Using Statements
using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Storage;
using Microsoft.Xna.Framework.GamerServices;
#endregion

namespace TeacherBase
{
    public class MonofootSlider
    {
        public int X;
        public int Y;
        public int Width;
        public int Height;
        public int scrubX;
        public int scrubWidth;
        public int scrubHeight;

        public bool isDragging;

        public MonofootSlider() {
            X = 150;
            Y = 525;
            Width = 200;
            Height = 20;
            scrubX = 100;
            scrubWidth = 20;
            scrubHeight = 64;
            isDragging = false;
        }


        public void moveSlider(int relativeX)
        {
            scrubX += relativeX;
            if (scrubX > 200)
            {
                scrubX = 200;
                isDragging = false;
            }
            else if(scrubX<0)
            {
                scrubX = 0;
                isDragging = false;
            }
               
        }

        public float updateRate()
        {
            float x = (scrubX);
            if (x < 100)
            {
                return (100 / x) * 1000;
            }
            else
            {
                float delta =  (100-(x-100)) +1;
                return (delta * 8);
            }

        }

        public bool isInBounds(MouseState newState)
        {
            return (this.X + this.scrubX < newState.X && newState.X < this.X + this.scrubX + this.scrubWidth && Math.Abs(newState.Y - this.Y) < this.scrubHeight / 2);
        }
    }
}
