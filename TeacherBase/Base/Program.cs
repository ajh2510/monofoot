﻿#region Using Statements
using System;
using System.Collections.Generic;
using System.Linq;
#endregion

namespace TeacherBase
{
#if WINDOWS || LINUX
    /// <summary>
    /// The main class.
    /// </summary>
    public static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            using (var game = new BaseGame()) {
                World w = new CrabWorld(400, 400, 1);
                game.setWorld(w);
                w.AddGraphics(game.graphics);
                game.Run();
            }
        }
    }
#endif
}
