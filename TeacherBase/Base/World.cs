﻿#region Using Statements
using System;
using System.Collections.Generic;
using System.IO;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Media;
using Microsoft.Xna.Framework.Storage;
using Microsoft.Xna.Framework.GamerServices;
using System.Reflection;
using System.Security.Permissions;
using System.Collections;
#endregion

namespace TeacherBase
{
    public class World
    {
        protected ArrayList characterList;
        public ContentManager Content;
        public GraphicsDeviceManager graphics;
        public SpriteBatch sb;
        public Microsoft.Xna.Framework.Graphics.SpriteFont font;
        protected int horizontalGridNumber, verticalGridNumber, gridWidth;
        protected float updateInterval = 1.00f;
        protected float currentTimeInterval = 0.00f;
        protected Vector2 textSize;
        protected Texture2D burd, lineTexture;
        protected Texture2D backgroundTexture;
        protected string backgroundTextureName;
        protected MonofootButton button1;
        protected MonofootSlider slider1;
        public SoundEffect fanfare;
        public SoundEffect slurp;
        public SoundEffect au;
        public MouseState oldState, newState;
        public KeyboardState keyboardState;
        protected GUIManager guiManager;
        public String[] characterNames;
        public Color bkgColor = Color.LightGray;

        public Color rectColor = Color.LightSlateGray;


        public World(int horizGridNum, int vertGridNum, int gridSize)
        {
            horizontalGridNumber = horizGridNum;
            verticalGridNumber = vertGridNum;
            gridWidth = gridSize;
            characterList = new ArrayList();
            button1 = new MonofootButton();
            slider1 = new MonofootSlider();
            
            
        }

        public void AddGraphics(GraphicsDeviceManager graphicsManager)
        {
            graphics = graphicsManager;
        }

        public void LoadContent(ContentManager newContentManager)
        {
            Content = newContentManager;
            font = Content.Load<Microsoft.Xna.Framework.Graphics.SpriteFont>("SpriteFont1");
            
            backgroundTexture = Content.Load<Texture2D>(backgroundTextureName);
            textSize = font.MeasureString("MonoGame");
            textSize = new Vector2(textSize.X / 4, textSize.Y / 4);
            //burd = Content.Load<Texture2D>("burd");
            lineTexture = Content.Load<Texture2D>("linetexture");
            fanfare= Content.Load<SoundEffect>("fanfare");
            au = Content.Load<SoundEffect>("au");
            slurp = Content.Load<SoundEffect>("slurp");
            guiManager = new GUIManager(this);
            Populate();
            Started();
            
            LoadCharacterList();
        }

        public virtual void Populate()
        {   

        }

        public void LoadCharacterList()
        {
            FileInfo currentDirectory = new FileInfo(AppDomain.CurrentDomain.BaseDirectory);
            DirectoryInfo mainDirectory = currentDirectory.Directory.Parent.Parent.Parent;
            String className = this.GetType().Name;
            bool foundDirectory = false;
            foreach (System.IO.DirectoryInfo f in mainDirectory.GetDirectories("*.*"))
            {
                if (className.Equals(f.Name))
                {
                    foundDirectory = true;
                }
            }

            if (foundDirectory)
            {
                mainDirectory = new DirectoryInfo(Path.Combine(mainDirectory.FullName, className));
                foreach (System.IO.FileInfo f in mainDirectory.GetFiles("*.*"))
                {
                    String curFileName=f.Name;
                    if (f.Name.EndsWith(".cs"))
                    {
                        curFileName = curFileName.Substring(0,curFileName.Length - 3);
                        if (!className.Equals(curFileName))
                        {
                            
                            //Console.WriteLine(curFileName);
                            Texture2D characterTexture = GetTexture(curFileName);
                            if (characterTexture != null)
                            {
                                Object[] methodParams = { curFileName };
                                Type characterType = Type.GetType("TeacherBase." + curFileName);
                                Character c=null;
                                try
                                {
                                    c= (Character)Activator.CreateInstance(characterType, methodParams);
                                }
                                catch (MissingMethodException e)
                                {
                                    try
                                    {
                                        c = (Character)Activator.CreateInstance(characterType, new Object[] { });
                                    }
                                    catch (Exception e2)
                                    {
                                        Console.WriteLine(e2.Message);
                                    }
                                }
                                if (c != null)
                                {
                                        guiManager.AddCharacterData(new CharacterDragData(c, characterTexture, curFileName));
                                }
                                else
                                {
                                    guiManager.AddNonCharacterClass(curFileName);
                                }
                                
                                
                            }
                            else
                            {
                                guiManager.AddNonCharacterClass(curFileName);
                            }
                        }
                        
                    }
                }
            }
        }

        public void Draw(GameTime gameTime, SpriteBatch spriteBatch)
        {
            sb = spriteBatch;
            DrawString(50, 19, "World");
            DrawRectangle(new Vector2(-5,-5), new Vector2(1000, 1000), bkgColor, true);

            if(gridWidth>1)
            DrawGrid();
            else
            {
                for (int i = 0; i < 400; i+=backgroundTexture.Width ){
                    for (int j = 0; j < 400; j += backgroundTexture.Height)
                    {
                        if (i + backgroundTexture.Width <= 400 && j + backgroundTexture.Height <= 400)
                        {
                            sb.Draw(backgroundTexture, new Rectangle(j + 50, i + 50, backgroundTexture.Width, backgroundTexture.Height), null, Color.White,
                            0.0f,     //angle of line (calulated above)
                            new Vector2(0, 0), // point in line about which to rotate
                            SpriteEffects.None,
                            0);
                        }
                        else
                        {
                            sb.Draw(backgroundTexture, new Rectangle(j + 50, i + 50, 400-i, 400-j), null, Color.White,
                            0.0f,     //angle of line (calulated above)
                            new Vector2(0, 0), // point in line about which to rotate
                            SpriteEffects.None,
                            0);
                        }
                    
                    }
                }
                    
            }

            DrawOutlines();

            foreach (var item in characterList)
            {
                DrawCharacter((Character)item);
            }

            DrawPlayButton();
            DrawSpeedSlider();
            guiManager.Draw(sb);
            /*sb.Draw(burd, new Rectangle(450, 50, burd.Width, burd.Height), null, Color.White,
                0,     //angle of line (calulated above)
                new Vector2(0, 0), // point in line about which to rotate
                SpriteEffects.None,
                0);*/
        }

        public void Update(GameTime gameTime)
        {
            keyboardState = Keyboard.GetState();
            newState = Mouse.GetState();
            currentTimeInterval += (float)gameTime.ElapsedGameTime.TotalMilliseconds / slider1.updateRate();
            if (currentTimeInterval >= updateInterval && !button1.isStopped)
            {
                for (int i = 0; i < characterList.Count; i++)
                {

                    Character c = (Character)characterList[i];
                    c.Update();
                }
               // FlipTheBurd();
                currentTimeInterval = 0.00f;
            }



            if (slider1.isDragging)
            {
                if(Math.Abs(newState.Y- slider1.Y )>32 || newState.X<slider1.X || newState.X>slider1.X+slider1.Width +20)
                {
                    slider1.isDragging = false;
                }
                slider1.moveSlider(newState.X - oldState.X);
            }

            //left mouse button down
            if (newState.LeftButton == ButtonState.Pressed && oldState.LeftButton == ButtonState.Released)
            {
                if (slider1.isInBounds(newState))
                {
                    slider1.isDragging = true;
                }
            }

            //left mouse button up
            if (newState.LeftButton == ButtonState.Released && oldState.LeftButton == ButtonState.Pressed){
                if(button1.isInBounds(oldState,newState))
                {
                    button1.isStopped = !button1.isStopped;
                    if (button1.isStopped)
                        Stopped();
                    else
                        Started();
                }

                slider1.isDragging = false;
            }

            //right mouse button down
            if (newState.RightButton == ButtonState.Pressed && oldState.RightButton == ButtonState.Released)
            {
                guiManager.displayingCharacterMenu= false;
                foreach (var item in characterList)
                {
                    Character currentCharacter = (Character)item;
                    if(currentCharacter.clickIntersects(newState)){
                        MethodInfo[] methodData = currentCharacter.GetMethodData();
                        guiManager.DisplayCharacterMenu(methodData, currentCharacter.GetType().Name, currentCharacter);
                    }
                }
            }

            //right mouse button up
            if (newState.RightButton == ButtonState.Released && oldState.RightButton == ButtonState.Pressed)
            {

            }

            guiManager.Update();


            oldState = newState;


        }

        public void DrawCharacter(Character character)
        {
            if (character.GetType().Name.Equals("Counter"))
            {
                Counter counter= (Counter)character;
                DrawString((int)counter.GetCoordinates().X, (int)counter.GetCoordinates().Y, counter.text + counter.value.ToString());
            }
            else
            {
                int characterX = ((int)character.GetCoordinates().X * gridWidth) + (gridWidth / 2) + 50;
                int characterY = ((int)character.GetCoordinates().Y * gridWidth) + (gridWidth / 2) + 50;
                if (character.GetTexture() == null)
                {
                    character.SetTexture(Content.Load<Texture2D>(character.GetTextureString()));
                }
                sb.Draw(character.GetTexture(), new Rectangle(characterX, characterY, character.GetTexture().Width, character.GetTexture().Height), null, Color.White,
                    character.GetRotation(),     //angle of line (calulated above)
                    new Vector2(character.GetTexture().Width / 2, character.GetTexture().Height / 2), // point in line about which to rotate
                    SpriteEffects.None,
                    0);
            }
        }

        public void DrawString(int textX, int textY, string toWrite){
            sb.DrawString(font, toWrite, new Vector2(textX, textY), Color.Black);
        }

        public void playFanfare()
        {
            slurp.Play(1.0f, 0.0f, 1.0f);
        }

        private void DrawGrid()
        {
            Vector2 startingLocation = new Vector2(50, 50);
            
            for(int i=0; i<verticalGridNumber*horizontalGridNumber; i++){
                int xVal = (int)startingLocation.X + (gridWidth * (i % horizontalGridNumber));
                int yVal = (int)startingLocation.Y + (gridWidth * (i / verticalGridNumber));
                sb.Draw(backgroundTexture, new Rectangle(xVal, yVal, gridWidth, gridWidth), null, Color.White,
                0.0f,     //angle of line (calulated above)
                new Vector2(0, 0), // point in line about which to rotate
                SpriteEffects.None,
                0);
            }

            for (int i = 0; i <= verticalGridNumber; i++)
            {
                int x1, y1, x2, y2;
                x1 = (int)startingLocation.X;
                y1 = (int)startingLocation.Y + (i * gridWidth);
                x2 = (int)startingLocation.X + (horizontalGridNumber * gridWidth);
                y2 = (int)startingLocation.Y + (i * gridWidth);

                DrawLine(new Vector2(x1, y1), new Vector2(x2, y2));
            }
            for (int j = 0; j <= horizontalGridNumber; j++)
            {
                int x1, y1, x2, y2;
                x1 = (int)startingLocation.X + (j * gridWidth);
                y1 = (int)startingLocation.Y;
                x2 = (int)startingLocation.X + (j * gridWidth);
                y2 = (int)startingLocation.Y + (verticalGridNumber * gridWidth);
                DrawLine(new Vector2(x1, y1), new Vector2(x2, y2));

            }

            

        }

        public void DrawLine(Vector2 start, Vector2 end)
        {
            Vector2 edge = end - start;
            // calculate angle to rotate line
            float angle =
                (float)Math.Atan2(edge.Y, edge.X);

            sb.Draw(lineTexture,
                new Rectangle(// rectangle defines shape of line and position of start of line
                    (int)start.X,
                    (int)start.Y,
                    (int)edge.Length(), //sb will strech the texture to fill this rectangle
                    2), //width of line, change this to make thicker line
                null,
                Color.Blue, //colour of line
                angle,     //angle of line (calulated above)
                new Vector2(0, 0), // point in line about which to rotate
                SpriteEffects.None,
                0);

        }

        public void FillRectangle(Vector2 start, Vector2 size, Color c)
        {
            sb.Draw(lineTexture,
                new Rectangle(// rectangle defines shape of line and position of start of line
                    (int)start.X,
                    (int)start.Y,
                    (int)size.X, //sb will strech the texture to fill this rectangle
                    (int)size.Y), //width of line, change this to make thicker line
                null,
                c, //colour of line
                0.0f,     //angle of line (calulated above)
                new Vector2(0, 0), // point in line about which to rotate
                SpriteEffects.None,
                0);
        }


        public void DrawRectangle(Vector2 start, Vector2 size, Color c, bool willFill)
        {
            if (willFill)
                FillRectangle(start, size, c);
            Vector2 topLeft = new Vector2(start.X, start.Y);
            Vector2 topRight = new Vector2(start.X+size.X, start.Y);
            Vector2 bottomLeft = new Vector2(start.X, start.Y+size.Y);
            Vector2 bottomRight = new Vector2(start.X+ size.X, start.Y + size.Y);
            DrawLine(topLeft, topRight);
            DrawLine(topLeft, bottomLeft);
            DrawLine(bottomLeft, bottomRight);
            DrawLine(topRight, bottomRight);
            
        }

        public void AddCharacter(Character c)
        {
            c.myWorld = this;
            characterList.Add(c);
            c.AddedToWorld();
        }

        public void Stop(){
            button1.isStopped = true;
        }


        public int GetWidth()
        {
            return horizontalGridNumber;
        }

        public float GetTotalWidth()
        {
            return horizontalGridNumber* gridWidth;
        }

        public float GetTotalHeight()
        {
            return verticalGridNumber * gridWidth;
        }

        public int GetHeight()
        {
            return verticalGridNumber;
        }

        public void DrawPlayButton()
        {
            Texture2D playButton;
            if (button1.isStopped)
            {
                playButton = Content.Load<Texture2D>("stopbutton");
            }
            else
            {
                playButton = Content.Load<Texture2D>("playbutton");
            }

            sb.Draw(playButton, new Rectangle(button1.X, button1.Y, button1.Width, button1.Height), null, Color.White,
               0.0f,     //angle of line (calulated above)
                new Vector2(0,0), // point in line about which to rotate
                SpriteEffects.None,
                0);
            
        }

        public void DrawSpeedSlider()
        {
            Texture2D sliderLine = Content.Load<Texture2D>("scrubline");
            Texture2D scrub = Content.Load<Texture2D>("scrub");
            sb.Draw(sliderLine, new Rectangle(slider1.X, slider1.Y, slider1.Width, slider1.Height), null, Color.White,
            0.0f,     //angle of line (calulated above)
            new Vector2(0, 0), // point in line about which to rotate
            SpriteEffects.None,
            0);

            sb.Draw(sliderLine, new Rectangle(slider1.X + slider1.scrubX, slider1.Y - (slider1.Height), slider1.scrubWidth, slider1.scrubHeight), null, Color.White,
            //sb.Draw(sliderLine, new Rectangle(600, 600, slider1.scrubWidth, slider1.scrubHeight), null, Color.White,
            0.0f,     //angle of line (calulated above)
            new Vector2(0, 0), // point in line about which to rotate
            SpriteEffects.None,
            0);



        }

        public void setBackground(string pictureName)
        {
            backgroundTextureName = pictureName;
        }

        public Texture2D GetBackground()
        {
            return backgroundTexture;
        }

        public int GetCellSize()
        {
            return gridWidth;
        }

        public ArrayList getCharacters()
        {
            return characterList;
        }

        public ArrayList GetCharacters(String name)
        {
            ArrayList toReturn = new ArrayList();
            foreach (Object curObject in characterList)
            {
                String typeName = curObject.GetType().Name;
                if (typeName.Equals(name))
                {
                    toReturn.Add(curObject);
                }

            }
            return toReturn;
        }

        public ArrayList getObjectsAt(int x, int y)
        {
            ArrayList toReturn = new ArrayList();
            foreach (var item in characterList)
            {
                Character curCharacter = (Character)item;
                if (curCharacter.GetX() == x && curCharacter.GetY() == y)
                {
                    toReturn.Add(curCharacter);
                }
            }

            return toReturn;
        }

        public void DrawOutlines()
        {
            DrawRectangle(new Vector2(50, 50),new Vector2((gridWidth * horizontalGridNumber),(gridWidth * verticalGridNumber)), rectColor, false);
            DrawRectangle(new Vector2(50, 495), new Vector2((gridWidth * horizontalGridNumber), 85), rectColor, true);
        }

        public int numberOfObjects()
        {
            return characterList.Count;
        }

        public void RemoveCharacter(Character toRemove)
        {
            for (int i = 0; i < characterList.Count; i++)
            {
                Character curCharacter = (Character)characterList[i];
                if (curCharacter.Equals(toRemove))
                {
                    curCharacter.RemovedFromWorld();
                    characterList.RemoveAt(i);
                    break;
                }
            }
        }

        public void RemoveCharacters(ArrayList toRemove)
        {
            for (int i = 0; i < toRemove.Count; i++)
            {
                RemoveCharacter((Character)toRemove[i]);
            }
        }

        public void Started()
        {

        }

        public void Stopped()
        {

        }

        public Texture2D GetTexture(string image)
        {
            Texture2D texture=null;
            try
            {
                texture = Content.Load<Texture2D>(image);
            }
            catch (Exception e)
            {
               // Console.WriteLine(e.Message);
            }
            
            return texture;
        }


        public void PlaySound(string sound)//monogame doesn't ssupport mp3s/wavs?
        {
            /*MediaPlayer.Volume = 1.0f;
            SoundEffect sf = Content.Load<SoundEffect>(sound);
            sf.Play();*/
        }

        public void SetBackground(Texture2D newBackground)
        {
            backgroundTexture = newBackground;
        }

        public void SetBackground(String newTextureName)
        {
            backgroundTexture = Content.Load<Texture2D>(newTextureName);
        }

        public ArrayList GetObjectsWithinDistance(int distance, bool diagonal, Character c)
        {
            ArrayList toReturn = new ArrayList();
            foreach (var item in characterList)
            {
                Character curCharacter = (Character)item;
                int offsetx = Math.Abs((int)c.GetX() - (int)curCharacter.GetX());
                int offsety = Math.Abs((int)c.GetY() - (int)curCharacter.GetY());

                if (diagonal)
                {
                    if (Math.Max(offsetx, offsety) <= distance)
                    {
                        toReturn.Add(curCharacter);
                    }
                }
                else
                {
                    if (offsetx +offsety <= distance)
                    { 
                        toReturn.Add(curCharacter);
                    }
                }
            }

            return toReturn;
        }

        public Character GetOneIntersectingObject(Character c)
        {
            Character toReturn = null;
            foreach (var item in characterList)
            {
                Character curCharacter = (Character)item;

                if (curCharacter.GetX() == c.GetX() && curCharacter.GetY() == c.GetY())
                {
                    if(!c.Equals(curCharacter))
                     toReturn= curCharacter;
                }
            }
            return toReturn;
        }

        private void FlipTheBurd()
        {
            Color[] burdData = new Color[burd.Width * burd.Height];
            burd.GetData<Color>(burdData);
            Color[,] burdDataAsGrid = new Color[burd.Height, burd.Width];
            Color[,] burdDataAsGrid2 = new Color[burd.Height, burd.Width];
            for (int row = 0; row < burd.Height; row++)
            {
                for (int column = 0; column < burd.Width; column++)
                {
                    // Assumes row major ordering of the array.
                    burdDataAsGrid[row, column] = burdData[row * burd.Width + column];
                }
            }

            for (int row = 0; row < burd.Height; row++)
            {
                for (int column = 0; column < burd.Width; column++)
                {
                    // Assumes row major ordering of the array.
                    burdDataAsGrid2[row, column] = burdDataAsGrid[(burd.Height - 1) - row, column];
                }
            }

            for (int row = 0; row < burd.Height; row++)
            {
                for (int column = 0; column < burd.Width; column++)
                {
                    // Assumes row major ordering of the array.
                    burdData[row * burd.Width + column] = burdDataAsGrid2[row, column];
                }
            }

            burd.SetData<Color>(burdData);
        }

    }
}
