﻿#region Using Statements
using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Storage;
using Microsoft.Xna.Framework.GamerServices;
using System.Collections;
#endregion

namespace TeacherBase
{
    public class Animal : Character
    {
        public float movementAmount = 5.0f;
        protected Random rnd1 = new Random();

        public Animal(string newTexture): base (newTexture)
        {

        }

        public Animal(string newTexture, int newX, int newY)
            : base(newTexture, newX, newY)
        {

        }

        public Animal() : base() { }
        public void Move()
        {
            float newX = (float)(movementAmount * Math.Cos(GetRotation()));
            float newY = (float)(movementAmount * Math.Sin(GetRotation()));
            SetLocation((int)(GetX() + newX), (int)(GetY() + newY));
        }

        public override void Update()
        {

        }

        public void Turn(int degrees)
        {
            SetRotation((GetRotation()* 180f/3.14159f) + (float)degrees);
            while (GetRotation() > 2f * 3.14159f)
            {
                SetRotation(GetRotation() - (2f * 3.14159f));
            }
        }

        public bool AtWorldEdge()
        {
            float minx = 50.0f, maxx = myWorld.GetTotalWidth()-50f, miny = 50.0f, maxy = myWorld.GetTotalHeight()-50f;
            if (GetX() < minx)
            {
                if(GetRotation()> (2f * 3.141f)- 0.8f || GetRotation()< 0.8f){
                    return false;
                }
                return true;
            }
            else if (GetX() > maxx)
            {
                if (Math.Abs(GetRotation() - 3.141f) < .8f)
                {
                    return false;
                }
                return true;
            }
            else if (GetY() < miny)
            {
                if (Math.Abs(GetRotation() - (3.141f /2)) < .8f)
                {
                    return false;
                }
                return true;
            }
            else if (GetY() > maxy)
            {
                if (Math.Abs(GetRotation() - (3.141f * 3 / 2)) < .8f)
                {
                    return false;
                }
                return true;
            }
            else
            {
                return false;
            }
        }

        public bool CanSee(String name)
        {
            ArrayList a = GetObjectsInRange(50);
            foreach(Object curObject in a){
                String typeName = curObject.GetType().Name;
                if(typeName.Equals(name)){
                    return true;
                }
                    
            }
            return false;
        }

        public void Eat(String name)
        {
            ArrayList a = GetObjectsInRange(50);
            Character toRemove = null;
            foreach (Object curObject in a)
            {
                String typeName = curObject.GetType().Name;
                if (typeName.Equals(name))
                {
                    toRemove = (Character)curObject;
                }

            }
            if(toRemove!= null)
            myWorld.RemoveCharacter(toRemove);
        }

        public void RandomTurn()
        {
            if (rnd1.Next(100) < 10)
            {
                Turn(rnd1.Next(-45, 45));
            }
        }

        public void TurnAtEdge()
        {
            if (AtWorldEdge())
            {
                Turn(17);
            }
        }



    }

  
}
