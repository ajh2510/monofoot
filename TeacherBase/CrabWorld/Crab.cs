﻿#region Using Statements
using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Media;
using Microsoft.Xna.Framework.Storage;
using Microsoft.Xna.Framework.GamerServices;
using System.Collections;
#endregion

namespace TeacherBase
{
    class Crab: Animal
    {
        Texture2D crab1;
        Texture2D crab2;

        public Crab()
            : base()
        {
        }

        public override void Update()
        {
            TurnAtEdge();

            RandomTurn();
            Move();
            LookForWorm();

            if (GetTexture().Equals(crab1))
            {
                SetTexture(crab2);
            }
            else
            {
                SetTexture(crab1);
            }
        }

        public void LookForWorm()
        {
            if (CanSee("Worm"))
            {
                Eat("Worm");
            }
        }

        public override void AddedToWorld()
        {
            crab1 = myWorld.GetTexture("crab");
            crab2 = myWorld.GetTexture("crab2");
            base.AddedToWorld();
        }
    }
}
