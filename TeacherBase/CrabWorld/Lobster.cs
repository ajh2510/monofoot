﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TeacherBase
{
    class Lobster : Animal
    {


        int wormsEaten = 0;

        public Lobster(string newTexture)
            : base(newTexture)
        {

        }

        public Lobster(string newTexture, int newX, int newY)
            : base(newTexture, newX, newY)
        {

        }

        public override void Update()
        {
            TurnAtEdge();
            Move();
            var keyboardState = myWorld.keyboardState;
            var keys = keyboardState.GetPressedKeys();

            if (keys.Length > 0)
            {
                string keyValue = keys[0].ToString();
                if (keyValue.Equals("Left"))
                {
                    Turn(-5);
                }
                else if (keyValue.Equals("Right"))
                {
                    Turn(5);
                }
            }

            LookForWorm();
        }

        public void LookForWorm()
        {
            if (CanSee("Worm"))
            {
                Eat("Worm");
                wormsEaten++;
                if (wormsEaten >= 8)
                {
                    myWorld.Stop();
                }
            }
        }
    }
}
