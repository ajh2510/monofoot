﻿#region Using Statements
using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Storage;
using Microsoft.Xna.Framework.GamerServices;
#endregion

namespace TeacherBase
{
    public class Wombat : Character
    {
        enum Direction { NORTH, WEST, SOUTH, EAST};

        private int direction;
        private int updateCount;

        public Wombat(ref World newWorld,string newTexture) :base(newTexture)
        {
            direction = 0;
            updateCount = 1;
        }

        public Wombat(string newTexture, int newX, int newY)
            : base(newTexture,newX,newY)
        {
            direction = 0;
            updateCount = 1;

        }

        public override void Update()
        {
            updateCount++;
            if (updateCount > 4) {
                Wombat w = new Wombat("textcharacter", 5, 5);
                //myWorld.addCharacter(w);
                //updateCount = -5;


            }        

            if (CanMove())
            {
               Move();
            }
            else
            {
                TurnLeft();
            }
        }
        public bool FoundLeaf()
        {
            return true;
        }

        public void EatLeaf()
        {

        }

        public void Move()
        {
            if (!CanMove())
            {
                return;
            }
            switch (direction)
            {
                case (int)Direction.SOUTH:
                    SetLocation(GetX(), GetY() + 1);
                    break;
                case (int)Direction.EAST:
                    SetLocation(GetX() + 1, GetY());
                    break;
                case (int)Direction.NORTH:
                    SetLocation(GetX(), GetY() - 1);
                    break;
                case (int)Direction.WEST:
                    SetLocation(GetX() - 1, GetY());
                    break;
            }


        }

        private bool CanMove()
        {
            World myWorld = GetWorld();
            int x = (int)coordinates.X;
            int y = (int)coordinates.Y;
            switch (direction)
            {
                case (int)Direction.SOUTH:
                    y++;
                    break;

                case (int)Direction.EAST:
                    x++;
                    break;

                case (int)Direction.NORTH:
                    y--;
                    break;

                case (int)Direction.WEST:
                    x--;
                    break;
            }

            if (x >= myWorld.GetWidth() || y >= myWorld.GetHeight())
            {
                return false;
            }
            else if (x < 0 || y < 0)
            {
                return false;
            }
            return true;
        }

        public void TurnLeft()
        {
            switch (direction)
            {
                case (int)Direction.SOUTH:
                    SetDirection((int)Direction.EAST);
                    break;
                case (int)Direction.EAST:
                    SetDirection((int)Direction.NORTH);
                    break;
                case (int)Direction.NORTH:
                    SetDirection((int)Direction.WEST);
                    break;
                case (int)Direction.WEST:
                    SetDirection((int)Direction.SOUTH);
                    break;

            }
        }

        public void SetDirection(int newDirection)
        {
            direction = newDirection;
            switch (direction)
            {
                case (int)Direction.SOUTH:
                    SetRotation(90);
                    break;
                case (int)Direction.EAST:
                    SetRotation(0);
                    break;
                case (int)Direction.NORTH:
                    SetRotation(270);
                    break;
                case (int)Direction.WEST:
                    SetRotation(180);
                    break;

            }
        }

    }




}
