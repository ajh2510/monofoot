﻿#region Using Statements
using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Media;
using Microsoft.Xna.Framework.Storage;
using Microsoft.Xna.Framework.GamerServices;
using System.Reflection;
using System.Security.Permissions;
using System.Collections;
#endregion
namespace TeacherBase
{

    class Body : SmoothMover
    {
        private double GRAVITY = 5.8;
        private Color defaultColor = new Color(255, 216, 0);
        private int imageSize;
        // fields
        double mass;

        /**
     * Construct a Body with default size, mass, movement and color.
     */
        public Body()
        {
            this.mass = 300;
            AddForce(movement);
            imageSize=20;
        }

        /**
         * Construct a Body with a specified size, mass, movement and color.
         */
        public Body(int newX, int newY,int size, double mass, Vector movement, Color color):base (newX,newY, movement)
        {
            this.mass = mass;
            imageSize = size;
            defaultColor = color;
        }

        public override void Update()
        {
            ApplyForces();
            Move();
        }

        public void ApplyForces()
        {
            ArrayList characterList = myWorld.GetCharacters("Body");
            foreach (Object curObject in characterList)
            {
                Body curBody = (Body)curObject;
                if (curBody != this)
                {
                    ApplyGravity(curBody);
                }

            }
        }

        public void ApplyGravity(Body other)
        {
            double dx = other.GetExactX() - this.GetExactX();
            double dy = other.GetExactY() - this.GetExactY();
            Vector force = new Vector(dx, dy);
            double distance = Math.Sqrt((dx * dx) + (dy * dy));
            double strength = GRAVITY * this.mass * other.mass / (distance * distance);
            double acceleration = strength / this.mass;
            force.SetLength(acceleration);
            AddForce(force);
        }



        public override void AddedToWorld()
        {
            texture = new Texture2D(myWorld.graphics.GraphicsDevice, imageSize, imageSize);
            FillOval(ref texture, 0, 0, imageSize-1, imageSize-1, defaultColor);
            SetTexture(texture);
            base.AddedToWorld();
        }

        public double GetMass()
        {
            return mass;
        }

        public void FillOval(ref Texture2D image, int x1, int y1, int x2, int y2, Color ovalColor)
        {

            int CenterX = ((x2 + x1) / 2);
            int CenterY = ((y2 + y1) / 2);
            double radius2 = (double)x2-CenterX;

            Color[] imageData = new Color[image.Width * image.Height];
            image.GetData<Color>(imageData);
            for (int i = x1; i < x2; i++)
            {
                for (int j = y1; j < y2; j++)
                {
                    double distanceTo = Math.Sqrt(((j - CenterY) * (j - CenterY)) + ((i - CenterX) * (i - CenterX)));
                    if (distanceTo < radius2)
                    {
                        imageData[j*image.Height +  i] = ovalColor;
                    }
                    else
                    {
                        imageData[j * image.Height + i] = Color.Transparent;
                    }
                }
            }

            image.SetData<Color>(imageData);
        }




    }
}
