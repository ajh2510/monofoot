﻿#region Using Statements
using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Storage;
using Microsoft.Xna.Framework.GamerServices;
using System.Collections;
#endregion

namespace TeacherBase
{
    public abstract class SmoothMover : Character
    {
        protected Vector movement;
        protected double exactX;
        protected double exactY;

        public SmoothMover()
        {
            movement = new Vector();
            exactX = 0;
            exactY = 0;
        }

        /**
         * Create new thing initialised with given speed.
         */
        public SmoothMover(int newX, int newY, Vector movement): base(newX, newY)
        {
            this.movement = movement;
            exactX = (double)newX;
            exactY = (double)newY;
        }

        /**
         * Move in the current movement direction.
         */
        public void Move()
        {
            exactX = exactX + movement.GetX();
            exactY = exactY + movement.GetY();
            SetLocation((int)exactX, (int)exactY);
        }

        /**
         * Set the location using exact (double) co-ordinates.
         */
        public void SetLocation(double x, double y)
        {
            exactX = x;
            exactY = y;
            SetLocation((float)x, (float)y);
        }

        /**
         * Set the location of this actor. Redefinition of the standard Greenfoot 
         * method to make sure the exact co-ordinates are updated in sync.
         */
        public void SetLocation(float x, float y)
        {
            exactX = (int)x;
            exactY = (int)y;
            base.SetLocation(x, y);
        }

        /**
         * Return the exact x co-ordinate (as a double).
         */
        public double GetExactX()
        {
            return exactX;
        }

        /**
         * Return the exact y co-ordinate (as a double).
         */
        public double GetExactY()
        {
            return exactY;
        }

        /**
         * Modify the current movement by adding a new vector to the existing movement.
         */
        public void AddForce(Vector force)
        {
            movement.Add(force);
        }

        /**
         * Accelerate the speed of this mover by the given factor. (Factors less than 1 will
         * decelerate.) The direction remains unchanged.
         */
        public void Accelerate(double factor)
        {
            movement.Scale(factor);
            if (movement.getLength() < 0.15)
            {
                movement.SetNeutral();
            }
        }

        /**
         * Return the speed of this actor.
         */
        public double GetSpeed()
        {
            return movement.getLength();
        }

        /**
         * Return the current movement of this object (as a vector).
         */
        public Vector GetMovement()
        {
            return movement;
        }
    }
}
