﻿#region Using Statements
using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Media;
using Microsoft.Xna.Framework.Storage;
using Microsoft.Xna.Framework.GamerServices;
using System.Reflection;
using System.Security.Permissions;
using System.Collections;
#endregion

namespace TeacherBase
{
    class SpaceWorld:World
    {
         public SpaceWorld(int horizGridNum, int vertGridNum, int gridSize): base(horizGridNum, vertGridNum, gridSize){
            setBackground("space");
        }

        public override void Populate()
        {

            //addCharacter(new Body("",50,50,100,0.0,new Vector(),Color.Red));

            AddCharacter(new Body(20, 250, 10, 50.0, new Vector(2.0, 1.0), Color.Green));
            AddCharacter(new Body(40, 260, 10, 50.0, new Vector(2.0, 5.0), Color.Green));
            AddCharacter(new Counter("Test: ", 300,300));


            Type[] theList = Assembly.GetExecutingAssembly().GetTypes();
            for (int i = 0; i < theList.Length; i++)
            {
                Console.WriteLine(theList[i].ToString());
            }
        }
    }
}
