﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TeacherBase
{
    public class Vector
    {
        double dx, dy;
        int direction;
        double length;

        public Vector()
        {

        }
        /**
     * Create a vector with given direction and length. The direction should be in
     * the range [0..359], where 0 is EAST, and degrees increase clockwise.
     */
        public Vector(int direction, double length)
        {
            this.length = length;
            this.direction = direction;
            UpdateCartesian();
        }

        /**
         * Create a vector by specifying the x and y offsets from start to end points.
         */
        public Vector(double dx, double dy)
        {
            this.dx = dx;
            this.dy = dy;
            UpdatePolar();
        }

        public void SetDirection(int direction)
        {
            this.direction = direction;
            UpdateCartesian();
        }

        /**
         * Add another vector to this vector.
         */
        public void Add(Vector other)
        {
            dx += other.dx;
            dy += other.dy;
            UpdatePolar();
        }

        /**
         * Set the length of this vector, leaving the direction intact.
         */
        public void SetLength(double length)
        {
            this.length = length;
            UpdateCartesian();
        }

        /**
         * Scale this vector up (factor greater than 1) or down (factor less than 1). 
         * The direction remains unchanged.
         */
        public void Scale(double factor)
        {
            length = length * factor;
            UpdateCartesian();
        }

        public void SetNeutral()
        {
            dx = 0.0;
            dy = 0.0;
            length = 0.0;
            direction = 0;
        }

        /**
         * Revert to horizontal component of this movement vector.
         */
        public void RevertHorizontal()
        {
            dx = -dx;
            UpdatePolar();
        }

        /**
         * Revert to vertical component of this movement vector.
         */
        public void RevertVertical()
        {
            dy = -dy;
            UpdatePolar();
        }

        /**
         * Return the x offset of this vector (start to end point).
         */
        public double GetX()
        {
            return dx;
        }

        public double GetY()
        {
            return dy;
        }


        public int GetDirection()
        {
            return direction;
        }
        public double getLength()
        {
            return length;
        }

        private void UpdateCartesian()
        {
            dx = length * Math.Cos(direction* Math.PI /180);
            dy = length * Math.Sin(direction * Math.PI / 180);
        }

        private void UpdatePolar()
        {
            this.direction = (int)((180 / Math.PI) *Math.Atan2(dy, dx));
            this.length = Math.Sqrt((dx * dx) + (dy * dy))/2;
        }   
    }
}
